package ru.tsc.gavran.tm.repository;

import ru.tsc.gavran.tm.api.repository.ITaskRepository;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public Task findByName(String userId, final String name) {
        return findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task removeByName(String userId, final String name) {
        final Optional<Task> task =  Optional.ofNullable(findByName(userId, name));
        task.ifPresent(this::remove);
        return task.orElseThrow(ProcessException::new);
    }

    @Override
    public Task startById(String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(final String userId, final int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final String userId, final int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(final String userId, final String name, final Status status) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final int index, final Status status) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(String userId, String id) {
        return findAll(userId).stream()
                .filter(t -> t.getUserId().equals(userId))
                .filter(t -> t.getProjectId().equals(id))
                .collect(Collectors.toList());
    }

    @Override
    public Task bindTaskToProjectById(String userId, String projectId, String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(String userId, String id) {
        final Task task = findById(userId, id);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void unbindAllTaskByProjectId(String userId, String id) {
        findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(id))
                .filter(t -> t.getUserId().equals(userId))
                .forEach(t -> t.setProjectId(null));
    }

}