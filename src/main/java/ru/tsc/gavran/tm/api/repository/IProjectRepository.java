package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IOwnerRepository<Project> {

    Project findByName(String userId, String name);

    Project removeByName(String userId, String name);

    Project startById(String userId, String id);

    Project startByName(String userId, String name);

    Project startByIndex(String userId, int index);

    Project finishById(String userId, String id);

    Project finishByName(String userId, String name);

    Project finishByIndex(String userId, int index);

    Project changeStatusById(String userId, String id, Status status);

    Project changeStatusByName(String userId, String name, Status status);

    Project changeStatusByIndex(String userId, int index, Status status);


}