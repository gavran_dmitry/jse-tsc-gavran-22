package ru.tsc.gavran.tm.api;

import ru.tsc.gavran.tm.model.AbstractEntity;

import java.awt.print.PrinterException;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(final E entity);

    void remove(final E entity);

    List<E> findAll();

    void clear();

    E findById(final String id);

    E removeById(final String id);

    E findByIndex(final Integer index);

    List<E> findAll(final Comparator<E> comparator);

    boolean existsById(final String id);

    E removeByIndex(final Integer index);

}
