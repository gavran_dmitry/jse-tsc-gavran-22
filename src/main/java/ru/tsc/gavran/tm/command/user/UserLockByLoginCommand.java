package ru.tsc.gavran.tm.command.user;

import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class UserLockByLoginCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER LOGIN: ");
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        final String login = TerminalUtil.nextLine();
        final String id = serviceLocator.getUserService().findById(login).getId();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("USER LOCK");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}