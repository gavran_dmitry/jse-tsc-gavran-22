package ru.tsc.gavran.tm.exception.empty;

import ru.tsc.gavran.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error. Index is empty");
    }

    public EmptyIndexException(String value) {
        super("Error." + value + " index is empty");
    }

}
